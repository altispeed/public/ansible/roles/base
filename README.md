<img style="float: left;" src="docs/assets/logo.png" alt="logo" width="300"/>

## <div style="text-align: right"></div>
Common Base for all server deployments

## Table of content

- [Default Variables](#default-variables)
  - [base_additional_ports](#base_additional_ports)
  - [base_admin](#base_admin)
  - [base_admin_password](#base_admin_password)
  - [base_key_group](#base_key_group)
  - [base_keys_source](#base_keys_source)
  - [base_reboot](#base_reboot)
  - [base_server_environment](#base_server_environment)
  - [base_snapd_enabled](#base_snapd_enabled)
  - [base_timezone](#base_timezone)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Default Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):


### base_additional_ports

#### Default Value

```YAML
base_additional_ports:
  - port: 22
    proto: tcp
    application: ssh
```

### base_admin

#### Default Value

```YAML
base_admin: localadmin
```

### base_admin_password

#### Default Value

```YAML
base_admin_password: changem3
```

### base_key_group

#### Default Value

```YAML
base_key_group: '{{ base_server_environment }}'
```

### base_keys_source

#### Default Value

```YAML
base_keys_source: file
```

### base_reboot

#### Default Value

```YAML
base_reboot: true
```

### base_server_environment

#### Default Value

```YAML
base_server_environment: dev
```

### base_snapd_enabled

#### Default Value

```YAML
base_snapd_enabled: false
```

### base_timezone

#### Default Value

```YAML
base_timezone: Etc/UTC
```

## Discovered Tags

**_always_**

**_install-updates_**

**_lockdown_**

**_setup-admin_**

**_setup-all_**

**_setup-ssh_**

**_update-base_**


## Dependencies

None.

## License

GPL-3.0-only

## Author

Dark Decoy

